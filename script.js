document.getElementById("enviar").addEventListener("click", () => {

    checker = true;


    //NICKNAME
    let usernameIn = document.getElementById("usernameIn");
    let cleanUsername = usernameIn.value.replace(/\s/g, "");
    checkForm(usernameIn, cleanUsername, "su Nombre de Usuario",);

    //NOMBRE
    let nameIn = document.getElementById("nameIn");
    let cleanName = nameIn.value.replace(/\s/g, "");
    checkForm(nameIn, cleanName, "su Nombre");

    //APELLIDO
    let surnameIn = document.getElementById("surnameIn");
    let cleanSurname = surnameIn.value.replace(/\s/g, "");
    checkForm(surnameIn, cleanSurname, "su Apellido");

    //EMAIL
    let emailIn = document.getElementById("emailIn");
    let cleanEmail = emailIn.value.replace(/\s/g, "");
    checkForm(emailIn, cleanEmail, "su Correo Electrónico");

    //DOMICILIO
    let addressIn = document.getElementById("addressIn");
    let cleanAddress = addressIn.value.replace(/\s/g, "");
    checkForm(addressIn, cleanAddress, "su Domicilio");

    //CONTRASEÑA
    let passwordIn = document.getElementById("passwordIn");
    let cleanPassword = passwordIn.value.replace(/\s/g, "");
    checkForm(passwordIn, cleanPassword, "su Contraseña");

    //RE CONTRASEÑA
    let repPassIn = document.getElementById("repPassIn");
    let cleanRepPass = repPassIn.value.replace(/\s/g, "");
    checkForm(repPassIn, cleanRepPass, "Repetir la Contraseña");

    //VALIDACION DE LOS DATOS DEL FORM
    if (checker == false) {
        return document.getElementById("errMsg").textContent = `Ups. Hay datos inválidos en el registro… `;
    } else if (checker == true) {
        document.getElementById("errMsg").textContent = ``;
    }

    let passComparison = comparator(cleanPassword, cleanRepPass);

    //COMPARANDO CONTRASEÑAS
    if (passComparison == false) {
        return document.getElementById("errMsg").textContent = `Ups.Las contraseñas no parecen coincidir…  `;
    } else if (passComparison == true) {

        //TODO BIEN, CREANDO EL USUARIO
        
        document.getElementById("errMsg").textContent = `ASD`;
        let tempUser = new User(cleanUsername, cleanName, cleanSurname, cleanEmail, addressIn.value.trim(), cleanPassword);
        verifyUser(cleanUsername, cleanEmail, tempUser, tempUser);
    }
});



//LOGIN

document.getElementById("sendLogin").addEventListener("click", () => {

    let usernameLogin = document.getElementById("usernameLogin");

    let passwordLogin = document.getElementById("passwordLogin");
    searchUserLogin(usernameLogin.value.replace(/\s/g, ""), passwordLogin.value.replace(/\s/g, ""));

}); 