

let clients = [];
let admins = [];

class User {
    constructor(username, name, surname, email, address, password) {
        this.username = username;
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.address = address;
        this.password = password;
    }
}

admins.push(new User("NOWA","NOWA","MIMOSA","nowa@gmail.com","CASITA","BONAFO"));
admins.push(new User("KURUMI","KURUMI","MIMOSA","kurumi@gmail.com","CASITA","ZAKFIEL"));
admins.push(new User("TINA","TINA","MIMOSA","tina@gmail.com","CASITA","ZAKFIEL"));


clients.push(new User("HANAKO","HANAKO","MIMOSA","hanako@gmail.com","CASITA","LILLY"));
clients.push(new User("EMI","EMI","MIMOSA","emi@gmail.com","CASITA","KORRA"));
clients.push(new User("LILLY","LILLY","MIMOSA","lilly@gmail.com","CASITA","LILLY"));

